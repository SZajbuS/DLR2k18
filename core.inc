#define BLD "r3"

#define SLOTS 30
#define START_MONEY 1500

#undef MAX_PLAYERS
#define MAX_PLAYERS SLOTS

//Database
new DB;
new Cache:result;
new q[256];

new str[512];

//Players Enum
enum e_PlayerData
{
	ID,
	Money
}

new pData[MAX_PLAYERS][e_PlayerData];

enum e_VehicleData
{
	Speed,
	Gear
}

new pVehicle[MAX_PLAYERS][e_VehicleData];


#include "modules/colors.inc"
#include "modules/textdraws.inc"
#include "modules/textdraws_func.inc"
#include "modules/basic_func.inc"
#include "modules/drift.inc"
#include "modules/commands.inc"