#include <a_samp>
#include <zcmd>
#include <sscanf2>
#include <a_mysql>
#include <streamer>

#include "core.inc"

main()
{
	print("\n----------------------------------");
	print(" driftLegends:Resurrection 2k18 by SZajbuS");
	print("----------------------------------\n");
}



public OnGameModeInit()
{

	//Database connection
	DB = mysql_connect("localhost", "root", "samp", "");
		if(mysql_errno() != 0)
			print("!= Nie udalo sie polaczyc z baza danych");
		else
			print("!= Polaczono z baza danych");

	SetGameModeText("[PL]DRiFT/4FuN");
	
	for(new i=0; i < 299; i++)
		AddPlayerClass(i, 1505.0670, -892.1097, 58.7301, 262.0000, 0, 0, 0, 0, 0, 0);
	
	loadTextdraws();
	setTimers();
	loadTextdraws();

	ManualVehicleEngineAndLights();
	DisableInteriorEnterExits();
	UsePlayerPedAnims();
	EnableStuntBonusForAll(0);


	return 1;
}

public OnGameModeExit()
{
	mysql_close(DB);
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	TogglePlayerSpectating(playerid, 0);
	SetPlayerInterior(playerid, 0);

	SetCameraBehindPlayer(playerid);
	SetPlayerPos(playerid, 1078.2152,2092.1687,10.8203);
	ApplyAnimation(playerid,"DANCING","dnce_M_d",1022.0,1,0,0,0,-1);
	SetPlayerFacingAngle(playerid, 311.8744);

	SetPlayerCameraPos(playerid, 1080.2152,2094.1687,11.2203);
	SetPlayerCameraLookAt(playerid,1078.2152,2092.1687,10.8203);
	return 1;
}

public OnPlayerConnect(playerid)
{
	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
	return 1;
}

public OnPlayerSpawn(playerid)
{
	SetPlayerPos(playerid,2326.9143,1392.2225,42.8203);
	SetPlayerInterior(playerid, 0);
	ClearAnimations(playerid);
	SetPlayerHealth(playerid, 99999999);
	GivePlayerWeapon(playerid, WEAPON_CAMERA, 1000);
	return 1;
}

public OnPlayerDeath(playerid, killerid, reason)
{
	return 1;
}

public OnVehicleSpawn(vehicleid)
{
	return 1;
}

public OnVehicleDeath(vehicleid, killerid)
{
	DestroyVehicle(vehicleid);
	return 1;
}

public OnPlayerText(playerid, text[])
{
	return 1;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
	if (strcmp("/mycommand", cmdtext, true, 10) == 0)
	{
		// Do something here
		return 1;
	}
	return 0;
}

public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	return 1;
}

public OnPlayerExitVehicle(playerid, vehicleid)
{
	hideDriftTD(playerid);
	hideVehicleTD(playerid);

	pVehicle[playerid][Gear] = 0;
	return 1;
}

public OnPlayerStateChange(playerid, newstate, oldstate)
{
	if(newstate == PLAYER_STATE_DRIVER)
		if(IsCar(GetPlayerVehicleID(playerid)))
		{
			showDriftTD(playerid);
			updateVehicleModelPreview(playerid);
			showVehicleTD(playerid);

			pVehicle[playerid][Gear] = 0;
		}
	return 1;
}

public OnPlayerEnterCheckpoint(playerid)
{
	return 1;
}

public OnPlayerLeaveCheckpoint(playerid)
{
	return 1;
}

public OnPlayerEnterRaceCheckpoint(playerid)
{
	return 1;
}

public OnPlayerLeaveRaceCheckpoint(playerid)
{
	return 1;
}

public OnRconCommand(cmd[])
{
	return 1;
}

public OnPlayerRequestSpawn(playerid)
{
	showMainTD(playerid);
	SetPlayerTime(playerid, 20, 0);
	TogglePlayerSpectating(playerid, 0);
	StopAudioStreamForPlayer(playerid);
	return 1;
}

public OnObjectMoved(objectid)
{
	return 1;
}

public OnPlayerObjectMoved(playerid, objectid)
{
	return 1;
}

public OnPlayerPickUpPickup(playerid, pickupid)
{
	return 1;
}

public OnVehicleMod(playerid, vehicleid, componentid)
{
	return 1;
}

public OnVehiclePaintjob(playerid, vehicleid, paintjobid)
{
	return 1;
}

public OnVehicleRespray(playerid, vehicleid, color1, color2)
{
	return 1;
}

public OnPlayerSelectedMenuRow(playerid, row)
{
	return 1;
}

public OnPlayerExitedMenu(playerid)
{
	return 1;
}

public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid)
{
	return 1;
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	
	if(IsPlayerInAnyVehicle(playerid))
	{
		new car = GetPlayerVehicleID(playerid);
		if(IsCar(car) && (oldkeys & 4 || oldkeys & 1))
		{
			RemoveVehicleComponent(car, 1010);
			AddVehicleComponent(car, 1010);
		}
	}
	
	//if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER && newkeys == KEY_SUBMISSION)
		//return cmd_flip(playerid, "");
		
	return 1;
}

public OnRconLoginAttempt(ip[], password[], success)
{
	return 1;
}

public OnPlayerUpdate(playerid)
{
	return 1;
}

public OnPlayerStreamIn(playerid, forplayerid)
{
	return 1;
}

public OnPlayerStreamOut(playerid, forplayerid)
{
	return 1;
}

public OnVehicleStreamIn(vehicleid, forplayerid)
{
	return 1;
}

public OnVehicleStreamOut(vehicleid, forplayerid)
{
	return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	return 1;
}

public OnPlayerClickPlayer(playerid, clickedplayerid, source)
{
	return 1;
}

public OnPlayerCommandReceived(playerid, cmdtext[])
{	
	return 1;
}

public OnPlayerCommandPerformed(playerid, cmdtext[], success)
{
	if(!success)
	{
		msgToPlayer(playerid, "taka komenda nie istnieje, wpisz /cmd");
	}
	return 1;
}
