#define C_PLAYER 0x8F8F8FFF
#define c_player "8F8F8F"

#define C_ADMIN 0xD90000FF
#define c_admin "D90000"

#define C_MOD 0xFF6600FF
#define c_mod "FF6600"

#define C_VIP 0x26D977FF
#define c_vip "26D977"

#define C_WHITE 0xFFFFFFFF
#define c_white "FFFFFF"
#define C_BLACK 0x000000FF
#define c_black "000000"

#define C_RED 0xFF0000FF
#define c_red "FF0000"
#define C_GREEN 0x00FF00FF
#define c_green "00FF00"
#define C_BLUE 0x0000FFFF
#define c_blue "0000FF"

#define C_LIME 0xB8FF00FF
#define c_lime "B8FF00"
#define C_YELLOW 0xFFFF42FF
#define c_yellow "FFFF42"
#define C_ORANGE 0xFF9D00FF
#define c_orange "FF9d00"
#define C_CYAN 0x00FFFFFF
#define c_cyan "00FFFF"
#define C_PINK 0xFF0078FF
#define c_pink "FF0078"
#define C_PURPLE 0xA60098FF
#define c_purple "A60098"
#define C_GRAY 0xC1C1C1FF
#define c_gray "C1C1C1"
