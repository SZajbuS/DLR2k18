//Main textdraws
new Text:mainTextdraws[6];
new Text:timeTextdraw;

new Text:moneyTD[MAX_PLAYERS];
new Text:driftcoinTD[MAX_PLAYERS];
new Text:idTD[MAX_PLAYERS];
//Vehicle textdraws
new Text:vehicleTextdraws[7];

new Text:modelPreview[MAX_PLAYERS];
new Text:speedTD[MAX_PLAYERS];
new Text:gearTD[MAX_PLAYERS];

//Drift textdraws
new Text:driftTitleTD;
new Text:driftDownTD;
new Text:driftTD[MAX_PLAYERS];

//Login textdraws
new Text:loginTextdraw[8];




stock loadTextdraws()
{
mainTextdraws[0] = TextDrawCreate(610.140014, 80.250000, "usebox");
TextDrawLetterSize(mainTextdraws[0], 0.000000, 1.760552);
TextDrawTextSize(mainTextdraws[0], 495.101013, 0.000000);
TextDrawAlignment(mainTextdraws[0], 1);
TextDrawColor(mainTextdraws[0], 0);
TextDrawUseBox(mainTextdraws[0], true);
TextDrawBoxColor(mainTextdraws[0], 255);
TextDrawSetShadow(mainTextdraws[0], 0);
TextDrawSetOutline(mainTextdraws[0], 0);
TextDrawFont(mainTextdraws[0], 0);

mainTextdraws[1] = TextDrawCreate(590.336730, 97.416702, "D");
TextDrawLetterSize(mainTextdraws[1], 0.609766, 1.419165);
TextDrawAlignment(mainTextdraws[1], 1);
TextDrawColor(mainTextdraws[1], -5963521);
TextDrawSetShadow(mainTextdraws[1], 0);
TextDrawSetOutline(mainTextdraws[1], 1);
TextDrawBackgroundColor(mainTextdraws[1], 30);
TextDrawFont(mainTextdraws[1], 2);
TextDrawSetProportional(mainTextdraws[1], 1);

mainTextdraws[2] = TextDrawCreate(594.554077, 92.750076, "i");
TextDrawLetterSize(mainTextdraws[2], 0.305695, 2.352503);
TextDrawAlignment(mainTextdraws[2], 1);
TextDrawColor(mainTextdraws[2], -5963521);
TextDrawSetShadow(mainTextdraws[2], 0);
TextDrawSetOutline(mainTextdraws[2], 0);
TextDrawBackgroundColor(mainTextdraws[2], 30);
TextDrawFont(mainTextdraws[2], 2);
TextDrawSetProportional(mainTextdraws[2], 1);

mainTextdraws[3] = TextDrawCreate(599.707397, 92.749969, "i");
TextDrawLetterSize(mainTextdraws[3], 0.284143, 2.364166);
TextDrawAlignment(mainTextdraws[3], 2);
TextDrawColor(mainTextdraws[3], -5963521);
TextDrawSetShadow(mainTextdraws[3], 0);
TextDrawSetOutline(mainTextdraws[3], 0);
TextDrawBackgroundColor(mainTextdraws[3], 210);
TextDrawFont(mainTextdraws[3], 2);
TextDrawSetProportional(mainTextdraws[3], 1);

mainTextdraws[4] = TextDrawCreate(610.141174, 100.666664, "usebox");
TextDrawLetterSize(mainTextdraws[4], 0.000000, 1.182764);
TextDrawTextSize(mainTextdraws[4], 533.051757, 0.000000);
TextDrawAlignment(mainTextdraws[4], 1);
TextDrawColor(mainTextdraws[4], 0);
TextDrawUseBox(mainTextdraws[4], true);
TextDrawBoxColor(mainTextdraws[4], 255);
TextDrawSetShadow(mainTextdraws[4], 0);
TextDrawSetOutline(mainTextdraws[4], 0);
TextDrawBackgroundColor(mainTextdraws[4], 203);
TextDrawFont(mainTextdraws[4], 0);

mainTextdraws[5] = TextDrawCreate(1.405480, 436.333343, "driftlegends.pl");
TextDrawLetterSize(mainTextdraws[5], 0.161859, 1.016666);
TextDrawAlignment(mainTextdraws[5], 1);
TextDrawColor(mainTextdraws[5], -1);
TextDrawSetShadow(mainTextdraws[5], 0);
TextDrawSetOutline(mainTextdraws[5], 1);
TextDrawBackgroundColor(mainTextdraws[5], 255);
TextDrawFont(mainTextdraws[5], 2);
TextDrawSetProportional(mainTextdraws[5], 1);

//Time textdraw
timeTextdraw = TextDrawCreate(546.295898, 53.666637, "21:37");
TextDrawLetterSize(timeTextdraw, 0.292575, 1.617499);
TextDrawAlignment(timeTextdraw, 1);
TextDrawColor(timeTextdraw, -1);
TextDrawSetShadow(timeTextdraw, 0);
TextDrawSetOutline(timeTextdraw, 1);
TextDrawBackgroundColor(timeTextdraw, 255);
TextDrawFont(timeTextdraw, 2);
TextDrawSetProportional(timeTextdraw, 1);

//Vehilce textdraws
vehicleTextdraws[0] = TextDrawCreate(732.423767, 447.166839, "usebox");
TextDrawLetterSize(vehicleTextdraws[0], 0.000000, -4.219626);
TextDrawTextSize(vehicleTextdraws[0], 549.917419, 0.000000);
TextDrawAlignment(vehicleTextdraws[0], 1);
TextDrawColor(vehicleTextdraws[0], 0);
TextDrawUseBox(vehicleTextdraws[0], true);
TextDrawBoxColor(vehicleTextdraws[0], 102);
TextDrawSetShadow(vehicleTextdraws[0], 0);
TextDrawSetOutline(vehicleTextdraws[0], 0);
TextDrawFont(vehicleTextdraws[0], 0);

vehicleTextdraws[1] = TextDrawCreate(571.127563, 417.083587, "~r~~h~KM/H");
TextDrawLetterSize(vehicleTextdraws[1], 0.164202, 1.016666);
TextDrawAlignment(vehicleTextdraws[1], 1);
TextDrawColor(vehicleTextdraws[1], -1);
TextDrawSetShadow(vehicleTextdraws[1], 0);
TextDrawSetOutline(vehicleTextdraws[1], 1);
TextDrawBackgroundColor(vehicleTextdraws[1], 51);
TextDrawFont(vehicleTextdraws[1], 2);
TextDrawSetProportional(vehicleTextdraws[1], 1);

vehicleTextdraws[2] = TextDrawCreate(587.651611, 437.833435, "usebox");
TextDrawLetterSize(vehicleTextdraws[2], 0.000000, -0.410370);
TextDrawTextSize(vehicleTextdraws[2], 576.155395, 0.000000);
TextDrawAlignment(vehicleTextdraws[2], 1);
TextDrawColor(vehicleTextdraws[2], 0);
TextDrawUseBox(vehicleTextdraws[2], true);
TextDrawBoxColor(vehicleTextdraws[2], -1061109505);
TextDrawSetShadow(vehicleTextdraws[2], 0);
TextDrawSetOutline(vehicleTextdraws[2], 0);
TextDrawFont(vehicleTextdraws[2], 0);

vehicleTextdraws[3] = TextDrawCreate(580.624267, 433.166534, "usebox");
TextDrawLetterSize(vehicleTextdraws[3], 0.000000, 0.048877);
TextDrawTextSize(vehicleTextdraws[3], 576.155700, 0.000000);
TextDrawAlignment(vehicleTextdraws[3], 1);
TextDrawColor(vehicleTextdraws[3], 0);
TextDrawUseBox(vehicleTextdraws[3], true);
TextDrawBoxColor(vehicleTextdraws[3], -1061109505);
TextDrawSetShadow(vehicleTextdraws[3], 0);
TextDrawSetOutline(vehicleTextdraws[3], 0);
TextDrawFont(vehicleTextdraws[3], 0);

vehicleTextdraws[4] = TextDrawCreate(587.651550, 433.166687, "usebox");
TextDrawLetterSize(vehicleTextdraws[4], 0.000000, 0.675922);
TextDrawTextSize(vehicleTextdraws[4], 583.183044, 0.000000);
TextDrawAlignment(vehicleTextdraws[4], 1);
TextDrawColor(vehicleTextdraws[4], 0);
TextDrawUseBox(vehicleTextdraws[4], true);
TextDrawBoxColor(vehicleTextdraws[4], -1061109505);
TextDrawSetShadow(vehicleTextdraws[4], 0);
TextDrawSetOutline(vehicleTextdraws[4], 0);
TextDrawFont(vehicleTextdraws[4], 0);

vehicleTextdraws[5] = TextDrawCreate(582.966918, 433.166564, "usebox");
TextDrawLetterSize(vehicleTextdraws[5], 0.000000, 0.647030);
TextDrawTextSize(vehicleTextdraws[5], 578.498107, 0.000000);
TextDrawAlignment(vehicleTextdraws[5], 1);
TextDrawColor(vehicleTextdraws[5], 0);
TextDrawUseBox(vehicleTextdraws[5], true);
TextDrawBoxColor(vehicleTextdraws[5], -1061109505);
TextDrawSetShadow(vehicleTextdraws[5], 0);
TextDrawSetOutline(vehicleTextdraws[5], 0);
TextDrawFont(vehicleTextdraws[5], 0);

vehicleTextdraws[6] = TextDrawCreate(585.309020, 433.166534, "usebox");
TextDrawLetterSize(vehicleTextdraws[6], 0.000000, 0.664991);
TextDrawTextSize(vehicleTextdraws[6], 580.840515, 0.000000);
TextDrawAlignment(vehicleTextdraws[6], 1);
TextDrawColor(vehicleTextdraws[6], 0);
TextDrawUseBox(vehicleTextdraws[6], true);
TextDrawBoxColor(vehicleTextdraws[6], -1061109505);
TextDrawSetShadow(vehicleTextdraws[6], 0);
TextDrawSetOutline(vehicleTextdraws[6], 0);
TextDrawFont(vehicleTextdraws[6], 1);

//Drift Textdraw
driftTitleTD = TextDrawCreate(298.916412, 414.750427, "~r~~h~DRIFT");
TextDrawLetterSize(driftTitleTD, 0.316939, 1.378332);
TextDrawAlignment(driftTitleTD, 1);
TextDrawColor(driftTitleTD, -1);
TextDrawSetShadow(driftTitleTD, 0);
TextDrawSetOutline(driftTitleTD, 1);
TextDrawBackgroundColor(driftTitleTD, 255);
TextDrawFont(driftTitleTD, 2);
TextDrawSetProportional(driftTitleTD, 1);

driftDownTD = TextDrawCreate(129.780364, 203.583175, "o");
TextDrawLetterSize(driftDownTD, 19.587219, 66.950859);
TextDrawAlignment(driftDownTD, 1);
TextDrawColor(driftDownTD, -2147483393);
TextDrawSetShadow(driftDownTD, 0);
TextDrawSetOutline(driftDownTD, 1);
TextDrawBackgroundColor(driftDownTD, 255);
TextDrawFont(driftDownTD, 1);
TextDrawSetProportional(driftDownTD, 1);

//Login Textdraw
loginTextdraw[0] = TextDrawCreate(-173.821304, -4.666667, "loadsc1:loadsc1");
TextDrawLetterSize(loginTextdraw[0], 0.000000, 0.000000);
TextDrawTextSize(loginTextdraw[0], 465.242248, 541.916625);
TextDrawAlignment(loginTextdraw[0], 1);
TextDrawColor(loginTextdraw[0], -1);
TextDrawSetShadow(loginTextdraw[0], 0);
TextDrawSetOutline(loginTextdraw[0], 0);
TextDrawFont(loginTextdraw[0], 4);

loginTextdraw[1] = TextDrawCreate(855.520446, -17.500001, "loadsc6:loadsc6");
TextDrawLetterSize(loginTextdraw[1], 0.000000, 0.000000);
TextDrawTextSize(loginTextdraw[1], -542.548095, 556.500122);
TextDrawAlignment(loginTextdraw[1], 1);
TextDrawColor(loginTextdraw[1], -1);
TextDrawSetShadow(loginTextdraw[1], 0);
TextDrawSetOutline(loginTextdraw[1], 0);
TextDrawFont(loginTextdraw[1], 4);

loginTextdraw[2] = TextDrawCreate(351.516784, -10.749994, "usebox");
TextDrawLetterSize(loginTextdraw[2], 0.000000, 53.677146);
TextDrawTextSize(loginTextdraw[2], 289.419982, 0.000000);
TextDrawAlignment(loginTextdraw[2], 1);
TextDrawColor(loginTextdraw[2], 0);
TextDrawUseBox(loginTextdraw[2], true);
TextDrawBoxColor(loginTextdraw[2], -2147483393);
TextDrawSetShadow(loginTextdraw[2], 0);
TextDrawSetOutline(loginTextdraw[2], 0);
TextDrawBackgroundColor(loginTextdraw[2], -2147483393);
TextDrawFont(loginTextdraw[2], 0);

loginTextdraw[3] = TextDrawCreate(296.231292, -0.249980, "usebox");
TextDrawLetterSize(loginTextdraw[3], 0.000000, 52.644508);
TextDrawTextSize(loginTextdraw[3], 289.888549, 0.000000);
TextDrawAlignment(loginTextdraw[3], 1);
TextDrawColor(loginTextdraw[3], 0);
TextDrawUseBox(loginTextdraw[3], true);
TextDrawBoxColor(loginTextdraw[3], -2139062017);
TextDrawSetShadow(loginTextdraw[3], 0);
TextDrawSetOutline(loginTextdraw[3], 0);
TextDrawFont(loginTextdraw[3], 0);

loginTextdraw[4] = TextDrawCreate(350.579589, -22.416629, "usebox");
TextDrawLetterSize(loginTextdraw[4], 0.000000, 77.273071);
TextDrawTextSize(loginTextdraw[4], 344.237121, 0.000000);
TextDrawAlignment(loginTextdraw[4], 1);
TextDrawColor(loginTextdraw[4], 0);
TextDrawUseBox(loginTextdraw[4], true);
TextDrawBoxColor(loginTextdraw[4], -2139062017);
TextDrawSetShadow(loginTextdraw[4], 0);
TextDrawSetOutline(loginTextdraw[4], 0);
TextDrawFont(loginTextdraw[4], 0);

loginTextdraw[5] = TextDrawCreate(250.190414, 86.916641, "~r~~h~drift~w~~h~Legends");
TextDrawLetterSize(loginTextdraw[5], 0.657554, 4.131666);
TextDrawAlignment(loginTextdraw[5], 1);
TextDrawColor(loginTextdraw[5], -1);
TextDrawSetShadow(loginTextdraw[5], 0);
TextDrawSetOutline(loginTextdraw[5], 1);
TextDrawBackgroundColor(loginTextdraw[5], 255);
TextDrawFont(loginTextdraw[5], 3);
TextDrawSetProportional(loginTextdraw[5], 1);

loginTextdraw[6] = TextDrawCreate(257.218322, 117.833366, "~w~~h~Resurrec~r~~h~tion");
TextDrawLetterSize(loginTextdraw[6], 0.244319, 1.640833);
TextDrawAlignment(loginTextdraw[6], 1);
TextDrawColor(loginTextdraw[6], -1);
TextDrawSetShadow(loginTextdraw[6], 0);
TextDrawSetOutline(loginTextdraw[6], 1);
TextDrawBackgroundColor(loginTextdraw[6], 255);
TextDrawFont(loginTextdraw[6], 2);
TextDrawSetProportional(loginTextdraw[6], 1);

loginTextdraw[7] = TextDrawCreate(344.362976, 117.833358, "~w~~h~2~r~~h~k~w~~h~18");
TextDrawLetterSize(loginTextdraw[7], 0.320219, 1.600000);
TextDrawAlignment(loginTextdraw[7], 1);
TextDrawColor(loginTextdraw[7], -1);
TextDrawSetShadow(loginTextdraw[7], 0);
TextDrawSetOutline(loginTextdraw[7], 1);
TextDrawBackgroundColor(loginTextdraw[7], 255);
TextDrawFont(loginTextdraw[7], 2);
TextDrawSetProportional(loginTextdraw[7], 1);

for(new i=0; i < MAX_PLAYERS; i++)
{
moneyTD[i] = TextDrawCreate(497.569885, 74.666580, "~r~~h~$~w~~h~00000000");
TextDrawLetterSize(moneyTD[i], 0.617730, 2.679168);
TextDrawAlignment(moneyTD[i], 1);
TextDrawColor(moneyTD[i], -1);
TextDrawSetShadow(moneyTD[i], 0);
TextDrawSetOutline(moneyTD[i], 0);
TextDrawBackgroundColor(moneyTD[i], 51);
TextDrawFont(moneyTD[i], 3);
TextDrawSetProportional(moneyTD[i], 1);

driftcoinTD[i] = TextDrawCreate(539.268371, 97.416664, "000000");
TextDrawLetterSize(driftcoinTD[i], 0.357701, 1.401665);
TextDrawAlignment(driftcoinTD[i], 1);
TextDrawColor(driftcoinTD[i], -1);
TextDrawSetShadow(driftcoinTD[i], 0);
TextDrawSetOutline(driftcoinTD[i], -3);
TextDrawFont(driftcoinTD[i], 2);
TextDrawSetProportional(driftcoinTD[i], 1);

idTD[i] = TextDrawCreate(587.994262, 58.333335, "ID: 69");
TextDrawLetterSize(idTD[i], 0.122503, 1.028333);
TextDrawAlignment(idTD[i], 1);
TextDrawColor(idTD[i], -1);
TextDrawSetShadow(idTD[i], 0);
TextDrawSetOutline(idTD[i], 1);
TextDrawBackgroundColor(idTD[i], 255);
TextDrawFont(idTD[i], 2);
TextDrawSetProportional(idTD[i], 1);

speedTD[i] = TextDrawCreate(571.127868, 414.166748, "185");
TextDrawLetterSize(speedTD[i], 0.204025, 1.407498);
TextDrawAlignment(speedTD[i], 3);
TextDrawColor(speedTD[i], -1);
TextDrawSetShadow(speedTD[i], 0);
TextDrawSetOutline(speedTD[i], 1);
TextDrawBackgroundColor(speedTD[i], 51);
TextDrawFont(speedTD[i], 2);
TextDrawSetProportional(speedTD[i], 1);

gearTD[i] = TextDrawCreate(577.686706, 425.833343, "~r~~h~N");
TextDrawLetterSize(gearTD[i], 0.455152, 1.984999);
TextDrawAlignment(gearTD[i], 3);
TextDrawColor(gearTD[i], -1);
TextDrawSetShadow(gearTD[i], 0);
TextDrawSetOutline(gearTD[i], 1);
TextDrawBackgroundColor(gearTD[i], 51);
TextDrawFont(gearTD[i], 2);
TextDrawSetProportional(gearTD[i], 1);

driftTD[i] = TextDrawCreate(318.594726, 424.666717, "2313");
TextDrawLetterSize(driftTD[i], 0.532927, 1.763333);
TextDrawAlignment(driftTD[i], 2);
TextDrawColor(driftTD[i], -1);
TextDrawSetShadow(driftTD[i], 0);
TextDrawSetOutline(driftTD[i], 1);
TextDrawBackgroundColor(driftTD[i], 255);
TextDrawFont(driftTD[i], 1);
TextDrawSetProportional(driftTD[i], 1);

//Vehpreview
modelPreview[i] = TextDrawCreate(590.336914, 378.000091, "_");
TextDrawLetterSize(modelPreview[i], 0.000000, 0.000000);
TextDrawTextSize(modelPreview[i], 61.844848, 71.750091);
TextDrawAlignment(modelPreview[i], 2);
TextDrawColor(modelPreview[i], -1);
TextDrawSetShadow(modelPreview[i], 0);
TextDrawSetOutline(modelPreview[i], 0);
TextDrawBackgroundColor(modelPreview[i], 0xFFFFFF00);
TextDrawFont(modelPreview[i], TEXT_DRAW_FONT_MODEL_PREVIEW);

}

}