CMD:v(pID, params[])
{
	new vID, vName[32];
	

	if(sscanf(params, "s[32]", vName))
		return msgToPlayer(pID, "użyj /v [nazwa lub id pojazdu]");
	else
	{
		vID = GetVehicleModelIDFromName(vName);
		if(vID == -1 )
        {
			vID = strval(vName);
			if(vID < 400 || vID > 611 )
			return msgToPlayer(pID, "nie ma takiego pojazdu");
        }
		if(vID == 425 || vID == 430 || vID == 432 || vID == 447 || vID == 476 || vID == 520)
		{
			
			
			msgToPlayer(pID, "nie możesz używać tego pojazdu");
			return 1;
			
		}
		new Float:x, Float:y, Float:z, Float:rot;
		GetPlayerPos(pID, x, y, z);
		GetPlayerFacingAngle(pID, rot);
		
		if(GetPlayerState(pID) == PLAYER_STATE_DRIVER)
			DestroyVehicle(GetPlayerVehicleID(pID));
			
		new v = CreateVehicle(vID, x, y, z, rot, random(100), random(100), -1);
		msgToPlayer(pID, "stworzyłeś pojazd!");
		PutPlayerInVehicle(pID, v, 0);
			
		SetVehicleVirtualWorld(v, GetPlayerVirtualWorld(pID));
		SetVehicleParamsEx(v, 1, 1, 0, 0, 0, 0, 0);
		return 1;
	
	}
}