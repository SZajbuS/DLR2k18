stock msgToPlayer(pID, text[])
{
	format(str, sizeof(str), "||{"c_white"}%s", text);
	SendClientMessage(pID, C_RED, str);

	return 1;
}

stock msgToAll(text[])
{
	format(str, sizeof(str), "||{"c_white"}%s", text);
	SendClientMessageToAll(C_RED, str);
	return 1;
}

stock isVehicleUsed(vID)
{
	for(new i=0;i<MAX_PLAYERS; i++)
	{
		if(GetPlayerVehicleID(i) == vID)
			return true;
	}

	return false;
}

stock tp(pID, Float:x, Float:y, Float:z, Float:rot)
{
	if(GetPlayerState(pID) == PLAYER_STATE_DRIVER)
	{
		new vID = GetPlayerVehicleID(pID);
		SetVehiclePos(vID, x, y, z);
		SetVehicleZAngle(vID, rot);
		PutPlayerInVehicle(pID, vID, 0);
	}
	else
	{
		SetPlayerPos(pID, x, y, z);
		SetPlayerFacingAngle(pID, rot);
	}
}

stock clearChat(pID)
	for(new i=0; i<25; i++)
		SendClientMessage(pID, WHITE, " ");
		
stock clearChatAll()
		for(new j = 0; j< 25; j++)
			SendClientMessageToAll(WHITE, " ");

stock IsCar(vid)
{
	new model = GetVehicleModel(vid);
	switch(model)
	{
		case 443:return 0;
		case 448:return 0;
		case 461:return 0;
		case 462:return 0;
		case 463:return 0;
		case 468:return 0;
		case 521:return 0;
		case 522:return 0;
		case 523:return 0;
		case 581:return 0;
		case 586:return 0;
		case 481:return 0;
		case 509:return 0;
		case 510:return 0;
		case 430:return 0;
		case 446:return 0;
		case 452:return 0;
		case 453:return 0;
		case 454:return 0;
		case 472:return 0;
		case 473:return 0;
		case 484:return 0;
		case 493:return 0;
		case 595:return 0;
		case 417:return 0;
		case 425:return 0;
		case 447:return 0;
		case 465:return 0;
		case 469:return 0;
		case 487:return 0;
		case 488:return 0;
		case 497:return 0;
		case 501:return 0;
		case 548:return 0;
		case 563:return 0;
		case 406:return 0;
		case 444:return 0;
		case 556:return 0;
		case 557:return 0;
		case 573:return 0;
		case 460:return 0;
		case 464:return 0;
		case 476:return 0;
		case 511:return 0;
		case 512:return 0;
		case 513:return 0;
		case 519:return 0;
		case 520:return 0;
		case 539:return 0;
		case 553:return 0;
		case 577:return 0;
		case 592:return 0;
		case 593:return 0;
		case 471:return 0;
	}
	return 1;
}

new VehicleNames[212][] = {
{"Landstalker"},{"Bravura"},{"Buffalo"},{"Linerunner"},{"Perrenial"},{"Sentinel"},{"Dumper"},
{"Firetruck"},{"Trashmaster"},{"Stretch"},{"Manana"},{"Infernus"},{"Voodoo"},{"Pony"},{"Mule"},
{"Cheetah"},{"Ambulance"},{"Leviathan"},{"Moonbeam"},{"Esperanto"},{"Taxi"},{"Washington"},
{"Bobcat"},{"Mr Whoopee"},{"BF Injection"},{"Hunter"},{"Premier"},{"Enforcer"},{"Securicar"},
{"Banshee"},{"Predator"},{"Bus"},{"Rhino"},{"Barracks"},{"Hotknife"},{"Trailer 1"},{"Previon"},
{"Coach"},{"Cabbie"},{"Stallion"},{"Rumpo"},{"RC Bandit"},{"Romero"},{"Packer"},{"Monster"},
{"Admiral"},{"Squalo"},{"Seasparrow"},{"Pizzaboy"},{"Tram"},{"Trailer 2"},{"Turismo"},
{"Speeder"},{"Reefer"},{"Tropic"},{"Flatbed"},{"Yankee"},{"Caddy"},{"Solair"},{"Berkley's RC Van"},
{"Skimmer"},{"PCJ-600"},{"Faggio"},{"Freeway"},{"RC Baron"},{"RC Raider"},{"Glendale"},{"Oceanic"},
{"Sanchez"},{"Sparrow"},{"Patriot"},{"Quad"},{"Coastguard"},{"Dinghy"},{"Hermes"},{"Sabre"},
{"Rustler"},{"ZR-350"},{"Walton"},{"Regina"},{"Comet"},{"BMX"},{"Burrito"},{"Camper"},{"Marquis"},
{"Baggage"},{"Dozer"},{"Maverick"},{"News Chopper"},{"Rancher"},{"FBI Rancher"},{"Virgo"},{"Greenwood"},
{"Jetmax"},{"Hotring"},{"Sandking"},{"Blista Compact"},{"Police Maverick"},{"Boxville"},{"Benson"},
{"Mesa"},{"RC Goblin"},{"Hotring Racer A"},{"Hotring Racer B"},{"Bloodring Banger"},{"Rancher"},
{"Super GT"},{"Elegant"},{"Journey"},{"Bike"},{"Mountain Bike"},{"Beagle"},{"Cropdust"},{"Stunt"},
{"Tanker"}, {"Roadtrain"},{"Nebula"},{"Majestic"},{"Buccaneer"},{"Shamal"},{"Hydra"},{"FCR-900"},
{"NRG-500"},{"HPV1000"},{"Cement Truck"},{"Tow Truck"},{"Fortune"},{"Cadrona"},{"FBI Truck"},
{"Willard"},{"Forklift"},{"Tractor"},{"Combine"},{"Feltzer"},{"Remington"},{"Slamvan"},
{"Blade"},{"Freight"},{"Streak"},{"Vortex"},{"Vincent"},{"Bullet"},{"Clover"},{"Sadler"},
{"Firetruck LA"},{"Hustler"},{"Intruder"},{"Primo"},{"Cargobob"},{"Tampa"},{"Sunrise"},{"Merit"},
{"Utility"},{"Nevada"},{"Yosemite"},{"Windsor"},{"Monster A"},{"Monster B"},{"Uranus"},{"Jester"},
{"Sultan"},{"Stratum"},{"Elegy"},{"Raindance"},{"RC Tiger"},{"Flash"},{"Tahoma"},{"Savanna"},
{"Bandito"},{"Freight Flat"},{"Streak Carriage"},{"Kart"},{"Mower"},{"Duneride"},{"Sweeper"},
{"Broadway"},{"Tornado"},{"AT-400"},{"DFT-30"},{"Huntley"},{"Stafford"},{"BF-400"},{"Newsvan"},
{"Tug"},{"Trailer 3"},{"Emperor"},{"Wayfarer"},{"Euros"},{"Hotdog"},{"Club"},{"Freight Carriage"},
{"Trailer 3"},{"Andromada"},{"Dodo"},{"RC Cam"},{"Launch"},{"Police Car (LSPD)"},{"Police Car (SFPD)"},
{"Police Car (LVPD)"},{"Police Ranger"},{"Picador"},{"S.W.A.T. Van"},{"Alpha"},{"Phoenix"},{"Glendale"},
{"Sadler"},{"Luggage Trailer A"},{"Luggage Trailer B"},{"Stair Trailer"},{"Boxville"},{"Farm Plow"},
{"Utility Trailer"}};

forward GetVehicleModelIDFromName(vname[]);
public GetVehicleModelIDFromName(vname[])
{
        for(new i = 0; i < 211; i++)
        {
                if ( strfind(VehicleNames[i], vname, true) != -1 )
                        return i + 400;
        }
        return -1;
}

stock setTimers()
{
	SetTimer("AngleUpdate" , 200, true);
	SetTimer("LicznikDriftu", 200, true);
	SetTimer("vehicleTimer", 150, true);
	SetTimer("timeUpdate", 10000, true);
	SetTimer("autoRepair", 1500, true);
}

stock getVehicleSpeed(vID)
{
	new Float:x, Float:y, Float:z, vel;
	GetVehicleVelocity(vID, x, y, z);
	vel = floatround( floatsqroot( x*x + y*y + z*z ) * 180 );
	return vel;
}

forward autoRepair();
public autoRepair()
{
	for(new i=0; i<MAX_PLAYERS; i++)
		if(GetPlayerState(i) == PLAYER_STATE_DRIVER)
			RepairVehicle(GetPlayerVehicleID(i));
}

forward timeUpdate();
public timeUpdate()
{
	new h, m, s;
	gettime(h, m, s);
	format(str, sizeof(str), "%02d:%02d", h, m);
	TextDrawSetString(timeTextdraw, str);
}

new Reverse[MAX_PLAYERS];

forward vehicleTimer();
public vehicleTimer()
{
	for(new i=0; i < MAX_PLAYERS; i++)
	{
		if(IsPlayerConnected(i))
		{
			if(GetPlayerState(i) == PLAYER_STATE_DRIVER)
			{
				new string[12];
				pVehicle[i][Speed] = getVehicleSpeed(GetPlayerVehicleID(i));
				valstr(string, pVehicle[i][Speed]);
				TextDrawSetString(speedTD[i], string);

				if(pVehicle[i][Speed] == 0)
					pVehicle[i][Gear] = 0;

				switch(pVehicle[i][Speed])
				{
				   	case 1..24: pVehicle[i][Gear] = 1;
				    case 25..64: pVehicle[i][Gear] = 2;
				    case 65..109: pVehicle[i][Gear] = 3;
				    case 110..159: pVehicle[i][Gear] = 4;
				    case 160..199: pVehicle[i][Gear] = 5;
				    case 200..229: pVehicle[i][Gear] = 6;
				    case 230..260: pVehicle[i][Gear] = 7;
				}

				new Keys, ud, lr;
				GetPlayerKeys(i, Keys, ud, lr);

				if(Keys == KEY_SPRINT) //UP
				{	
					Reverse[i] = 0;
				}
    			else if(Keys == KEY_JUMP) //DOWN
    			{
    				Reverse[i] += 1;

    				if(Reverse[i] >= 3)
    				{
    					pVehicle[i][Gear] = -1;
    				}
    			}
    			else if(Keys != KEY_SPRINT && Keys != KEY_JUMP)
    			{

    			}

    			switch(pVehicle[i][Gear])
    			{
    				case -1: string = "R";
    				case 0: string = "N";
    				case 1: string = "1";
    				case 2: string = "2";
    				case 3: string = "3";
    				case 4: string = "4";
    				case 5: string = "5";
    				case 6: string = "6";
    				case 7: string = "7";
    			}
    			TextDrawSetString(gearTD[i], string);

			}
		}
	}
}

stock updateVehicleModelPreview(pID)
{
	new vehModel = GetVehicleModel(GetPlayerVehicleID(pID));
	TextDrawSetPreviewModel(modelPreview[pID], vehModel);
	TextDrawSetPreviewRot(modelPreview[pID], -10.0, 0.0, -20.0, 0.8);
}