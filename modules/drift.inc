
#define DRIFT_MINKAT 10.0
#define DRIFT_MAXKAT 90.0
#define COLOR_LIGHTRED 0xFF0000FF
#define COLOR_LIGHTBLUE 0x33CCFFAA
#define DRIFT_SPEED 30.0

enum Float:Pos{ Float:sX,Float:sY,Float:sZ };
new DriftPointsNow[MAX_PLAYERS];
new Float:ppos[MAX_PLAYERS][3];
new PunktyDriftuGracza[MAX_PLAYERS];
forward LicznikDriftu();
forward PodsumowanieDriftu(playerid);
new Float:SavedPos[50][Pos];

forward AngleUpdate();
public AngleUpdate(){
	for(new g=0;g<200;g++){
		new Float:x, Float:y, Float:z;
		if(IsPlayerInAnyVehicle(g))GetVehiclePos(GetPlayerVehicleID(g), x, y, z); else GetPlayerPos(g, x, y, z);
		ppos[g][0] = x;
		ppos[g][1] = y;
		ppos[g][2] = z;
	}
}
Float:GetPlayerTheoreticAngle(i)
{
	new Float:sin;
	new Float:dis;
	new Float:angle2;
	new Float:x,Float:y,Float:z;
	new Float:tmp3;
	new Float:tmp4;
	new Float:MindAngle;
	if(IsPlayerConnected(i)){
		GetPlayerPos(i,x,y,z);
		dis = floatsqroot(floatpower(floatabs(floatsub(x,ppos[i][0])),2)+floatpower(floatabs(floatsub(y,ppos[i][1])),2));
		if(IsPlayerInAnyVehicle(i))GetVehicleZAngle(GetPlayerVehicleID(i), angle2); else GetPlayerFacingAngle(i, angle2);
		if(x>ppos[i][0]){tmp3=x-ppos[i][0];}else{tmp3=ppos[i][0]-x;}
		if(y>ppos[i][1]){tmp4=y-ppos[i][1];}else{tmp4=ppos[i][1]-y;}
		if(ppos[i][1]>y && ppos[i][0]>x){ //1
			sin = asin(tmp3/dis);
			MindAngle = floatsub(floatsub(floatadd(sin, 90), floatmul(sin, 2)), -90.0);
		}
		if(ppos[i][1]<y && ppos[i][0]>x){ //2
			sin = asin(tmp3/dis);
			MindAngle = floatsub(floatadd(sin, 180), 180.0);
		}
		if(ppos[i][1]<y && ppos[i][0]<x){ //3
			sin = acos(tmp4/dis);
			MindAngle = floatsub(floatadd(sin, 360), floatmul(sin, 2));
		}
		if(ppos[i][1]>y && ppos[i][0]<x){ //4
			sin = asin(tmp3/dis);
			MindAngle = floatadd(sin, 180);
		}
	}
	if(MindAngle == 0.0){
		return angle2;
	} else
		return MindAngle;
}

forward PodsumowanieDriftu(playerid);
public PodsumowanieDriftu(playerid){
	PunktyDriftuGracza[playerid] = 0;
	
	SetPlayerScore(playerid,GetPlayerScore(playerid) + DriftPointsNow[playerid]);
	SetPVarInt(playerid, "Score", GetPlayerScore(playerid));
	
	DriftPointsNow[playerid] = 0;
	TextDrawSetString(driftTD[playerid],"0");
	
	SetTimer("DMCDP",5000,false);
}

Float:ReturnPlayerAngle(playerid){
	new Float:Ang;
	if(IsPlayerInAnyVehicle(playerid))GetVehicleZAngle(GetPlayerVehicleID(playerid), Ang); else GetPlayerFacingAngle(playerid, Ang);
	return Ang;
}
public LicznikDriftu(){
	new Float:Angle1, Float:Angle2, Float:BySpeed, s[256];
	new Float:Z;
	new Float:X;
	new Float:Y;
	new Float:SpeedX;
	for(new g=0;g<MAX_PLAYERS;g++){
		GetPlayerPos(g, X, Y, Z);
		SpeedX = floatsqroot(floatadd(floatadd(floatpower(floatabs(floatsub(X,SavedPos[ g ][ sX ])),2),floatpower(floatabs(floatsub(Y,SavedPos[ g ][ sY ])),2)),floatpower(floatabs(floatsub(Z,SavedPos[ g ][ sZ ])),2)));
		Angle1 = ReturnPlayerAngle(g);
		Angle2 = GetPlayerTheoreticAngle(g);
		BySpeed = floatmul(SpeedX, 12);
		if(GetPlayerState(g) == PLAYER_STATE_DRIVER && IsCar(GetPlayerVehicleID(g)) && floatabs(floatsub(Angle1, Angle2)) > DRIFT_MINKAT && floatabs(floatsub(Angle1, Angle2)) < DRIFT_MAXKAT && BySpeed > DRIFT_SPEED){
			if(PunktyDriftuGracza[g] > 0)KillTimer(PunktyDriftuGracza[g]);
			PunktyDriftuGracza[g] = 0;
			DriftPointsNow[g] += ((floatval( floatabs(floatsub(Angle1, Angle2)) * 3 * (BySpeed*0.1) )/10) / 10);
			
			PunktyDriftuGracza[g] = SetTimerEx("PodsumowanieDriftu", 3000, 0, "d", g);
			
		}
		if(DriftPointsNow[g] > 0){
			
			format(s, sizeof(s), "%d", DriftPointsNow[g]);
			TextDrawSetString(driftTD[g], s);
		}
		SavedPos[ g ][ sX ] = X;
		SavedPos[ g ][ sY ] = Y;
		SavedPos[ g ][ sZ ] = Z;
	}
}
//Split(s1[], s2[], s3[]=""){new rxx[256];format(rxx, 256, "%s%s%s", s1, s2, s3);return rxx;}
//tostr(int){new st[256];format(st, 256, "%d", int);return st;}
floatval(Float:val){new str1[256];format(str1, 256, "%.0f", val);return todec(str1);}
todec(str1[]){return strval(str1);}