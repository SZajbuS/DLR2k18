stock showMainTD(pID)
{
	TextDrawShowForPlayer(pID, mainTextdraws[0]);
	TextDrawShowForPlayer(pID, mainTextdraws[1]);
	TextDrawShowForPlayer(pID, mainTextdraws[2]);
	TextDrawShowForPlayer(pID, mainTextdraws[3]);
	TextDrawShowForPlayer(pID, mainTextdraws[4]);
	TextDrawShowForPlayer(pID, mainTextdraws[5]);
	TextDrawShowForPlayer(pID, timeTextdraw);
	TextDrawShowForPlayer(pID, moneyTD[pID]);
	TextDrawShowForPlayer(pID, idTD[pID]);
	TextDrawShowForPlayer(pID, driftcoinTD[pID]);
}

stock hideMainTD(pID)
{
	TextDrawHideForPlayer(pID, mainTextdraws[0]);
	TextDrawHideForPlayer(pID, mainTextdraws[1]);
	TextDrawHideForPlayer(pID, mainTextdraws[2]);
	TextDrawHideForPlayer(pID, mainTextdraws[3]);
	TextDrawHideForPlayer(pID, mainTextdraws[4]);
	TextDrawHideForPlayer(pID, mainTextdraws[5]);
	TextDrawHideForPlayer(pID, timeTextdraw);
	TextDrawHideForPlayer(pID, moneyTD[pID]);
	TextDrawHideForPlayer(pID, idTD[pID]);
	TextDrawHideForPlayer(pID, driftcoinTD[pID]);
}

stock showVehicleTD(pID)
{
	TextDrawShowForPlayer(pID, vehicleTextdraws[0]);
	TextDrawShowForPlayer(pID, vehicleTextdraws[1]);
	TextDrawShowForPlayer(pID, vehicleTextdraws[2]);
	TextDrawShowForPlayer(pID, vehicleTextdraws[3]);
	TextDrawShowForPlayer(pID, vehicleTextdraws[4]);
	TextDrawShowForPlayer(pID, vehicleTextdraws[5]);
	TextDrawShowForPlayer(pID, vehicleTextdraws[6]);
	TextDrawShowForPlayer(pID, modelPreview[pID]);
	TextDrawShowForPlayer(pID, speedTD[pID]);
	TextDrawShowForPlayer(pID, gearTD[pID]);
}

stock hideVehicleTD(pID)
{
	TextDrawHideForPlayer(pID, vehicleTextdraws[0]);
	TextDrawHideForPlayer(pID, vehicleTextdraws[1]);
	TextDrawHideForPlayer(pID, vehicleTextdraws[2]);
	TextDrawHideForPlayer(pID, vehicleTextdraws[3]);
	TextDrawHideForPlayer(pID, vehicleTextdraws[4]);
	TextDrawHideForPlayer(pID, vehicleTextdraws[5]);
	TextDrawHideForPlayer(pID, vehicleTextdraws[6]);
	TextDrawHideForPlayer(pID, modelPreview[pID]);
	TextDrawHideForPlayer(pID, speedTD[pID]);
	TextDrawHideForPlayer(pID, gearTD[pID]);
}

stock showDriftTD(pID)
{
	TextDrawShowForPlayer(pID, driftTD[pID]);
	TextDrawShowForPlayer(pID, driftTitleTD);
	TextDrawShowForPlayer(pID, driftDownTD);
}

stock hideDriftTD(pID)
{
	TextDrawHideForPlayer(pID, driftTD[pID]);
	TextDrawHideForPlayer(pID, driftTitleTD);
	TextDrawHideForPlayer(pID, driftDownTD);
}

stock showLoginTD(pID)
{
	TextDrawShowForPlayer(pID, loginTD[0]);
	TextDrawShowForPlayer(pID, loginTD[1]);
	TextDrawShowForPlayer(pID, loginTD[2]);
	TextDrawShowForPlayer(pID, loginTD[3]);
	TextDrawShowForPlayer(pID, loginTD[4]);
	TextDrawShowForPlayer(pID, loginTD[5]);
	TextDrawShowForPlayer(pID, loginTD[6]);
	TextDrawShowForPlayer(pID, loginTD[7]);
}

stock hideLoginTD(pID)
{
	TextDrawHideForPlayer(pID, loginTD[0]);
	TextDrawHideForPlayer(pID, loginTD[1]);
	TextDrawHideForPlayer(pID, loginTD[2]);
	TextDrawHideForPlayer(pID, loginTD[3]);
	TextDrawHideForPlayer(pID, loginTD[4]);
	TextDrawHideForPlayer(pID, loginTD[5]);
	TextDrawHideForPlayer(pID, loginTD[6]);
	TextDrawHideForPlayer(pID, loginTD[7]);
}